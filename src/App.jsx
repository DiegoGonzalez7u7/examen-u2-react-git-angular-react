import { useState } from 'react'
import reactLogo from './assets/react.svg'
// una vez creado el proyecto se generan de manera 
// manual los componentes respetando su estructura de
// primera letra mayuscula
// dentro del componente usar la abreviatura rafc para generar su 
// estructura

import './App.css'
// importamos componentes
import ComponenteUno from './Components/ComponenteUno'
import ComponenteDos from './Components/ComponenteDos'
import ComponenteTres from './Components/ComponenteTres'
// importamos componentes

function App() {
  return (
    <>
      <div>
        <div className='c1'>
          {/* mandamos a llamar a los componentes */}
          <ComponenteUno /> 
        </div>
        <div className="c2">
          {/* mandamos a llamar a los componentes */}
          <ComponenteDos />
        </div>
        <div className="c3">
          {/* mandamos a llamar a los componentes */}
          <ComponenteTres />
        </div>
      </div>
    </>

  )
}

export default App
