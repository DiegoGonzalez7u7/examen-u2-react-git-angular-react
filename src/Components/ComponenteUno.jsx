import React from 'react'
import '../App.css'
let angular={ //objeto con propiedades 
  uno:'ng new app',
  dos:'ng serve',
  tres:'ng serve -o',
  cuatro:'ng g c components/componente',
  cinco:'ng g s service/servicio/rick',
  seis:'ng build',
}
function ComponenteUno() {
  return (
    <div>
      {/* recorrido manual */}
        <h1>Angular</h1>
        <h2>{angular.uno}</h2>
        <h2>{angular.dos}</h2>
        <h2>{angular.tres}</h2>
        <h2>{angular.cuatro}</h2>
        <h2>{angular.cinco}</h2>
        <h2>{angular.seis}</h2>
    </div>
  )
}

export default ComponenteUno