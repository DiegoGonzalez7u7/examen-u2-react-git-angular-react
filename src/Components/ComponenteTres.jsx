import React from 'react'
let git={ //objeto con propiedades
  uno:'git status',
  dos:'git add .',
  tres:'git commit -m "cambio"',
  cuatro:'git push',
  cinco:'git pull'
}
function ComponenteTres() {
  return (
    <div>
      {/* recorrido manual */}
      <h1>Git</h1>
      <h2>{git.uno}</h2>
      <h2>{git.dos}</h2>
      <h2>{git.tres}</h2>
      <h2>{git.cuatro}</h2>
      <h2>{git.cinco}</h2>
    </div>
  )
}

export default ComponenteTres