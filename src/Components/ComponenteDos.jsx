import React from 'react'
let react={ //objeto con propiedades
  uno:'npm create vite@latest',
  dos:'npx create aplicacion-uno',
  tres:'npm run dev',
  cuatro:'npm run build',
}
function ComponenteDos() {
  return (
    <div>
      {/* aqui no existe el ngfor se recorren de manera manual */}
      <h1>React</h1>
      <h2>{react.uno}</h2>
      <h2>{react.dos}</h2>
      <h2>{react.tres}</h2>
      <h2>{react.cuatro}</h2>
    </div>
  )
}

export default ComponenteDos

